#ifndef NODE_H
#define NODE_H

using namespace std;

struct Node{
    int index;
    struct Node* next;
    struct Node* prev;
    //distance to node "0"
    double d20;
    Node(int index, double d20):index(index),d20(d20),next(NULL), prev(NULL){};
};

#endif // NODE_H
