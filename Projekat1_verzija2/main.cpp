#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include "node.h"
#include <ctime>

using namespace std;

double distance(double x1, double y1, double x2, double y2){
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
}

vector<vector<double> > make_adj_matrix(vector<int>& indices, vector<double>& lat, vector<double>& lon){

    vector<vector<double> > adj_matrix;
    vector<double> temp;
    //initialize adj_matrix
    for(int i=0;i<indices.size();i++)
        temp.push_back(0);
    for(int i=0;i<indices.size();i++)
        adj_matrix.push_back(temp);

    //calculate adjacency matrix
    for(int i=0;i<indices.size();i++){
        for(int j=i+1;j<indices.size();j++){
            double d = distance(lat.at(i),lon.at(i),lat.at(j),lon.at(j));
            adj_matrix.at(i).at(j)=d;
            adj_matrix.at(j).at(i)=d;
        }
    }
    return adj_matrix;
}

bool is_cycle(Node* x, Node* y, bool traverse = false){
    Node* temp = x;
    Node* real_prev = x;

    if(temp->next != NULL){
        temp = temp->next;
    }else{
        temp = temp->prev;
    }

    if(traverse)
        cout<<real_prev->index<<"--";
    while(temp){

        if(traverse)
            cout<<temp->index<<"--";

        //cycle
        if(temp->index == y->index)
            return true;

        //check in which side to go next
        //check from which side we came
        if(temp->next && temp->next->index == real_prev->index){
            real_prev = temp;
            temp=temp->prev;
        }else{
            real_prev = temp;
            temp = temp->next;
        }

    }

    return false;

}

bool cmp(const Edge *e1, const Edge *e2)
{
    //function for sorting edges by savings
    return (e1->n1->d20 + e1->n2->d20- e1->d) > (e2->n1->d20 + e2->n2->d20 - e2->d);
}


int main()
{
    //read file

    ifstream infile("bih.txt");
    int a;
    double b,c;
    vector<int> indices;
    vector<double> latitude;
    vector<double> longitude;
    while (infile >> a >> b >> c)
    {
        indices.push_back(a);
        latitude.push_back(b);
        longitude.push_back(c);
    }

    //Make adjacency matrix
    vector<vector<double> > adj_matrix = make_adj_matrix(indices,latitude,longitude);

    //Create nodes
    vector<Node*> nodes;
    for(int i=1;i<indices.size();i++){
        Node* n = new Node(i,adj_matrix.at(0).at(i));
        nodes.push_back(n);
    }

    //Calculate cost at the beginning
    double s=0;
    for(int i=0;i<nodes.size();i++)
        s+= nodes.at(i)->d20;
    cout<<"Total cost: "<<2*s<<endl;

    //create all edges
    vector<Edge*> edges;
    for(int i=0;i<nodes.size();i++){
        for(int j=i+1;j<nodes.size();j++){
            Edge* e = new Edge(nodes.at(i), nodes.at(j),adj_matrix.at(nodes.at(i)->index).at( nodes.at(j)->index));
            edges.push_back(e);
        }
    }
    /*
    //measure time
    clock_t begin = clock();
    */
    sort(edges.begin(),edges.end(),cmp);
    /*
    //write time
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cout<<elapsed_secs<<endl;
    */

    /*
    //measure time
    begin = clock();
    */
    int k=0;
    int i=0;
    int n = nodes.size();
    s=0;
    while(k<n-1){

        //if n1 is connected at both side
        if(edges.at(i)->n1->next!=NULL &&edges.at(i)->n1->prev!=NULL){
            i++;
            continue;
        }
        //if n2 is connected at both side
        if(edges.at(i)->n2->next!=NULL &&edges.at(i)->n2->prev!=NULL){
            i++;
            continue;
        }

        // if node n1 is connected at one side
        if(edges.at(i)->n1->next!=NULL || edges.at(i)->n1->prev!=NULL){
            // if node n2 is connected at one side
            if(edges.at(i)->n2->next!=NULL || edges.at(i)->n2->prev!=NULL){
                //check for cycle
                if(is_cycle(edges.at(i)->n1,edges.at(i)->n2)){
                    i++;
                    continue;
                }
            }
        }

        //connect nodes
        if(edges.at(i)->n1->next==NULL)
            edges.at(i)->n1->next=edges.at(i)->n2;
        else
            edges.at(i)->n1->prev=edges.at(i)->n2;

        if(edges.at(i)->n2->next==NULL)
            edges.at(i)->n2->next = edges.at(i)->n1;
        else
            edges.at(i)->n2->prev = edges.at(i)->n1;

        s+=adj_matrix.at(edges.at(i)->n1->index).at(edges.at(i)->n2->index);

        i++;
        k++;

    }
    /*
    //write time
    end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cout<<elapsed_secs<<endl;
    */
    //find head and tail
    i=0;
    while(nodes.size()>2){
        if(nodes.at(i)->next!=NULL && nodes.at(i)->prev!=NULL)
            nodes.erase(nodes.begin()+i);
        else
            i++;
    }
    // add distance from node 0 to remaining nodes in vector nodes
    s+=nodes.at(0)->d20;
    s+=nodes.at(1)->d20;
    cout << "Final route cost: "<<s<<endl;

    //write route
    cout<<"0--";
    is_cycle(nodes.at(0),nodes.at(1),true);
    cout<<"0--";

    return 0;
}
