#ifndef NODE_H
#define NODE_H

using namespace std;

struct Node{
    int index;
    struct Node* next;
    struct Node* prev;
    //distance to node "0"
    double d20;

    Node(int index, double d20):index(index),d20(d20),next(NULL), prev(NULL){};
};

struct Edge{

    Node* n1;
    Node* n2;
    double d;
    Edge(Node* n1, Node* n2, double d):n1(n1), n2(n2), d(d){};

};

#endif // NODE_H
